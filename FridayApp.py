import kivy
from kivy.lang import Builder
from kivy.app import App
from kivy.uix.label import Label
from kivy.uix.screenmanager import ScreenManager, Screen

from kivy.uix.gridlayout import GridLayout

from Friday import Assistant
from CircularProgressBar import CircularProgressBar
from kivy.clock import Clock
import time
import threading

from multiprocessing import Process, Queue, Event

Builder.load_string('''
#:import Label kivy.core.text.Label
#:set _label Label(text="")
<Interface>:
    GridLayout:
        rows: 3
        GridLayout:
            cols: 2
            rows: 0
            ScrollView:
                size: self.size
                do_scroll_x: False
                Label:
                    id: friday_log
                    text_size: self.width,None
                    size_hint_y: None
                    height: self.texture_size[1]
                    width: self.texture_size[0]
                    font_size: root.height / 30
                    halign: "left"
            ScrollView:
                size: self.size
                do_scroll_x: False
                Label:
                    id: user_log
                    text_size: self.width,None
                    size_hint_y: None
                    height: self.texture_size[1]
                    width: self.texture_size[0]
                    font_size: root.height / 30
                    halign: "right"
        BoxLayout:
            size_hint_y: None
            height:50
            spacing: 15
            canvas:
                Color:
                    rgba: (0.746,0.8,0.86,1)
                Rectangle:
                    pos: self.pos
                    size: self.size
            TextInput:
                id:message
                hint_text: "Write to Friday"
                multiline: False
                on_text_validate: root.askText()

        FloatLayout:
            size_hint_y: None
            height:100
            spacing: 15
            Button:
                pos: self.pos
                background_color: 1, 0, 0, 1
                on_press: root.askVoice()

            CircularProgressBar:
                id: progress
                pos: self.width/2-40, self.height/6
                thickness: 5
                cap_style: "RouND"
                progress_colour: "010"
                background_colour: "001"
                cap_precision: 3
                max: 120
                min: 100
                widget_size: 80
                halign: "middle"
                label: _label

'''

)


class WikiSearch(threading.Thread):
    def __init__(self, question, assistant, callback, progress):
        self.question = question
        self.assistant = assistant
        self.callback = callback
        self.update_progress = progress
        threading.Thread.__init__(self)

    def run(self):
        answer = self.assistant.wiki.ask(question=self.question, callback=self.update_progress)
        if(answer is not None):
            self.callback(answer)

class Talk(threading.Thread):
    def __init__(self, statement, assistant):
        self.statement = statement
        self.assistant = assistant
        threading.Thread.__init__(self)

    def run(self):
        answer = self.assistant.talk(self.statement)
        if(answer is not None):
            return

class Listen(threading.Thread):
    def __init__(self, assistant, callback):
        self.assistant = assistant
        self.callback = callback
        threading.Thread.__init__(self)

    def run(self):
        listen = self.assistant.speech2T(self.callback)
        if(listen is not None):
            return

class Interface(Screen):
    def __init__(self, **kwargs):
        super(Interface, self).__init__(**kwargs)
        self.friday_log = self.ids["friday_log"]
        self.user_log = self.ids["user_log"]
        self.friday_log.text += "Hi, this is Friday\n\n"
        self.user_log.text += "\n\nTest Friday"

        self.assistant = Assistant()

        #Clock.schedule_interval(self.animate, 0.05)

    def askVoice(self):
        print('triggered')
        #self.friday_log.text += "Hi, this is Friday\n\n"
        self.activateProgress()
        listen = Listen(self.assistant, self.searchWikipedia)
        listen.start()
        #Clock.schedule_once(lambda dt: self.assistant.speech2T(self.searchWikipedia), 1)

    def addText(self, text):
        self.friday_log.text += text+"\n\n"

    def talk(self, text):
        self.addText(text)
        speech = Talk(text, self.assistant)
        speech.start()

    def searchWikipedia(self, question):
        #self.activateProgress()

        print('asdf')
        search = WikiSearch(question, self.assistant, self.talk, self.updateProgress)
        self.talk('I am searching wikipedia. Reading lots of articles. I will tell you a story. This is going to be very long. I am telling this story just to see whether if multitask is working or not.')
        search.start()

    def askText(self):
        print('triggered')

    def updateProgress(self):
        Clock.schedule_once(self.animate, 0)

    def activateProgress(self):
        delay = 0.05

        step = 0
        while step<20:
            Clock.schedule_once(self.animate, delay*step)
            step += 1

    def animate(self, dt):
        bar = self.ids["progress"]
        if bar.value < bar.max:
            bar.value += 1
        else:
            bar.value = bar.min


class Friday(App):
    def build(self):

        return sm

sm = ScreenManager()
sm.add_widget(Interface(name="main_screen"))
