import tensorflow as tf
import tensorflow_hub as hub
import sentencepiece as spm
from sklearn.metrics.pairwise import cosine_similarity
import numpy as np
from tqdm import tqdm

class transformerEmbed():
    def __init__(self):
        self.input_placeholder = tf.sparse_placeholder(tf.int64, shape=[None, None])
        self.sp = spm.SentencePieceProcessor()
        self.sp.Load('./universal_encoder_8k_spm.model')
        self.module = hub.Module("https://tfhub.dev/google/universal-sentence-encoder-lite/2")

        self.session = tf.Session()
        self.session.run(tf.global_variables_initializer())
        self.embeddings = self.module(inputs=dict(values=self.input_placeholder.values, indices=self.input_placeholder.indices,
        dense_shape=self.input_placeholder.dense_shape))

    def process_to_IDs_in_sparse_format(self, sp, sentences):
        ids = [sp.EncodeAsIds(x) for x in sentences]
        max_len = max(len(x) for x in ids)
        dense_shape=(len(ids), max_len)
        values=[item for sublist in ids for item in sublist]
        indices=[[row,col] for row in range(len(ids)) for col in range(len(ids[row]))]
        return (values, indices, dense_shape)

    def encodeSentences(self, sentences):
        values, indices, dense_shape = self.process_to_IDs_in_sparse_format(self.sp, sentences)

        embeddings = self.session.run(
        self.embeddings, feed_dict={self.input_placeholder.values: values,
        self.input_placeholder.indices: indices,
        self.input_placeholder.dense_shape: dense_shape})
        return embeddings

    def encodeParagraphs(self, paragraphs):
        sentenceBatch = ['']
        paragraphIndices = [] #each element is numpy format

        # print(paragraphs)

        totSentences = list(map(lambda paragraph:len(paragraph), paragraphs))
        maxLength = max(totSentences)

        paragraphIndices = np.zeros((len(paragraphs), maxLength), dtype=int)
        ID = 1

        with tqdm(total=len(paragraphs)) as pbar:
            for i in range(len(paragraphs)):
                indices = list(range(ID, ID+len(paragraphs[i])))
                paragraphIndices[i][:len(indices)] = indices
                ID += len(paragraphs[i])
                sentenceBatch.extend(paragraphs[i])
                pbar.update(1)

        # print('loaded paragraphIndices')
        #
        # print('sentenceBatch length: ', len(sentenceBatch))

        # print(sentenceBatch)

        sentenceEncoding = self.encodeSentences(sentenceBatch)

        paragraphs = sentenceEncoding[paragraphIndices]

        quotient = np.expand_dims(np.count_nonzero(paragraphIndices, axis=1), axis=1)

        quotient = np.where(quotient == 0, 1.0, quotient)

        paragraphVectors = np.sum(paragraphs, axis=1)

        paragraphVectors /= quotient
        return paragraphVectors
