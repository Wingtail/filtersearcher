from wikiSearch import WikiSearch

from gtts import gTTS
import speech_recognition as sr
import os
import webbrowser
#import smtplib
from pygame import mixer
import pygame as pg
import keyboard
from sifEmbed import SIF
from googletrans import Translator
import pickle
from sklearn.metrics.pairwise import cosine_similarity
import numpy as np
from multiprocessing import Queue

class Assistant:
    def __init__(self):
        self.mic = sr.Microphone()
        self.r = sr.Recognizer()
        self.wiki = WikiSearch()
        with self.mic as source:
            self.r.energy_threshold = 400

        vocabDict = pickle.load(open('sif_vocabDict.pckl', "rb"))
        vecLookup = pickle.load(open('sif_vecLookup.pckl', "rb"))
        freqLookup = pickle.load(open('sif_freqLookup.pckl', "rb"))

        params = {
        'alpha': 1e-3,
        'dim':300,
        'vocabulary': vocabDict,
        'vecLookup':vecLookup,
        'freqLookup':freqLookup,
        }

        self.sif = SIF(params)
        self.intention = [["Tell me more"]]
        self.intentionVec = self.sif.sifEmbedParagraphs(self.intention)
        print(self.intentionVec)
        # with self.mic as source:
        #     self.r.adjust_for_ambient_noise(self.mic, duration=1)

        self.language = 'en'
        self.translator = Translator()

    def engToKor(self, text):
        translated = self.translator.translate(text, dest='ko')
        return translated.text

    def koToEng(self, text):
        translated = self.translator.translate(text, src='ko')
        return translated.text

    def talk(self, audio):
        print(audio)
        mixer.init(27000)
        if(self.language == 'kr'):
            speech = gTTS(text=audio, lang='ko')
        else:
            speech = gTTS(text=audio, lang='en')
        speech.save('audio.mp3')
        clock = pg.time.Clock()
        try:
            mixer.music.load('audio.mp3')
        except pg.error:
            print('Speech error!')
        mixer.music.play()
        while mixer.music.get_busy():
            clock.tick(30)

        return True

    def speech2T(self, callback):
        self.talk('Yes?')
        with self.mic as source:
            print('Ready listening!')
            # self.r.adjust_for_ambient_noise(self.mic, duration=0.5)
            audio = self.r.listen(source)
        try:
            if(self.language == 'kr'):
                query = self.r.recognize_google(audio, language='ko')
                query = self.koToEng(query)
            else:
                query = self.r.recognize_google(audio)
            print(query)

        except sr.UnknownValueError:
            command = self.speech2T()
        query = query.split(" ")
        query = [word for word in query if word not in ["um","uh","uhh","umm"]]
        query = ' '.join(query)
        callback(query)
        return query

    def activate(self, phrase):
        with self.mic as source:
            try:
                self.r.adjust_for_ambient_noise(self.mic, duration=0.5)
                audio = self.r.listen(source)
                text = self.r.recognize_google(audio)
                if (text.lower() == phrase):
                    return True
                else:
                    return False
            except sr.UnknownValueError:
                self.r.adjust_for_ambient_noise(source)
                print('adjusting sound')

    def run(self):
        #Run BERT or any other classifier that comprehends speech
        #cross reference which command does the query best match
        self.talk('Activated')
        while True:
            if keyboard.is_pressed('q'):
                try:
                    if(self.language == 'kr'):
                        self.talk('네?')
                    else:
                        self.talk('Yes?')
                    query = self.speech2T()
                    queryEmbed = np.expand_dims(self.sif.sifEmbed([query], removeComponent=False), axis=0)
                    print(cosine_similarity(queryEmbed, self.intentionVec))
                    if(query == "bye"):
                        sp = "bye "+self.friend
                        self.talk(sp)
                        return
                    elif(cosine_similarity(queryEmbed, self.intentionVec)[0] > 0.7):
                        query = query.lower().replace('tell me more', '')
                        query = query.replace('summarize','')
                        query = query.replace('tell me','')
                        print(query)
                        summary = self.wiki.summarize(question=query)
                        self.talk(summary)
                    # self.talk('deep searching wikipedia...')
                    # self.talk('I will notify you when I am done searching')
                    else:
                        answer = self.wiki.ask(query.lower())
                        if(len(answer) > 0):
                            if(self.language == 'kr'):
                                answer = self.engToKor(answer)
                                self.talk(answer)
                            else:
                                self.talk(answer)
                        else:
                            if(self.language == 'kr'):
                                self.talk('답은 못찾았지만 비슷한 요약은 해볼게요')
                                self.wiki.summarize(query)
                            else:
                                self.talk('I cant find an answer, but I will try to summarize')
                                self.wiki.summarize(query)
                except sr.UnknownValueError:
                    self.talk('Please repeat that for me. I can not hear you')
                    command = speech2T()



# ass = Assistant()
# # ass.run()
# answer = ass.wiki.ask('Why do geese honk while flying'.lower())

# ass.talk(answer)
