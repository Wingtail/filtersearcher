import pickle
from FilterSearcher import FilterSearcher
from pytorch_pretrained_bert import BertTokenizer, BertForQuestionAnswering, BertModel
from tfidf_doc_ranker import TfidfDocRanker
from doc_db import DocDB

tfidf = TfidfDocRanker()
docdb = DocDB()

searcher = FilterSearcher()

articles, questions, answers = pickle.load(open('pipelineTest.pckl', 'rb'))

qAp = pickle.load(open('paragraphCheck.pckl', 'rb'))

correct = 0
em = 0
total = 0

accuracy = 0.0

# f = open('test.txt', 'a')
fm = open('EM.txt', 'a')

paragraphs = []

for article in articles:
    paragraphs.extend(article)

for paragraph in qAp.keys():
    if(paragraph not in paragraphs):
        print('error!')
        break

def evalAnswer():
    tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
    em = 0
    total = 0
    accuracy = 0.0
    for i in range(len(questions)):
        articles = tfidf.closest_docs(questions[i], k=1)[0]

        articles = [docdb.get_doc_text(article).split('\n') for article in articles]

        articles = [list(filter(None, article)) for article in articles]

        answer = searcher.evaluateFromArticles(articles, questions[i])
        print(questions[i])
        print(answers[i])
        print('answer: ',answer)

        if(answers[i] is not None):
            if(answer in [tokenizer.tokenize(x.lower()) for x in answers[i]]):
                print('EM!')
                em += 1
        elif(answers[i] is None and len(answer) <= 0):
            print('EM!')
            em+=1

        total += 1
        accuracy = em/float(total)

        print('accuracy: ', accuracy)

        fm.write('Accuracy '+str(i)+' '+str(accuracy)+'\n')

# def evalFilter():
#     correct = 0
#     total = 0
#
#     accuracy = 0.0
#     for i in range(len(questions)):
#         topK = searcher.evaluateFilter(articles, questions[i])
#
#         for paragraph in topK:
#             #print(paragraph)
#             if(questions[i] in qAp[paragraph]):
#                 print('correct!')
#                 correct += 1
#                 break
#
#         total += 1
#         # print(questions[i])
#         # print(answers[i])
#         # print('answer: ',answer)
#         #
#         # if(answers[i] is not None):
#         #     if(answer in [x.lower() for x in answers[i]]):
#         #         print('correct!')
#         #         correct += 1
#         # elif(answers[i] is None and len(answer) <= 0):
#         #     print('correct!')
#         #     correct+=1
#
#         accuracy = correct/float(total)
#
#         f.write('Accuracy: '+str(i)+' '+str(accuracy)+'\n')

# evalFilter()
evalAnswer()
# f.close()
fm.close()
