import pickle
from sifEmbed import SIF
from sklearn.metrics.pairwise import cosine_similarity
import numpy as np
import requests
from pytorch_pretrained_bert import BertTokenizer, BertForQuestionAnswering, BertModel
import nltk

vocabDict = pickle.load(open('sif_vocabDict.pckl', "rb"))
vecLookup = pickle.load(open('sif_vecLookup.pckl', "rb"))
freqLookup = pickle.load(open('sif_freqLookup.pckl', "rb"))

params = {
'alpha': 1e-3,
'dim':300,
'vocabulary': vocabDict,
'vecLookup':vecLookup,
'freqLookup':freqLookup,
}

# sif = SIF(params)

sentences = ['One of the most important foods in my life is pizza', 'I love pizza', 'Pizza is delicious', 'I live off of pizza']

phrase='I\'m one of the greatest showmans'

tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
context = BertModel.from_pretrained('bert-base-uncased')

a = tokenizer.tokenize(phrase)
print(a)

# payload = {'source':'en', 'target':'ko', 'text':'I love pizza'}
# r = requests.get('https://openapi.naver.com/v1/papago/n2mt', params=payload)
#
# print(r.text)
# embeddings = sif.sifEmbed(sentences)
# print(embeddings)
# sims = cosine_similarity(embeddings)
#
# avgSims = np.sum(sims, axis=1) / len(sims)
#
# print(avgSims)
