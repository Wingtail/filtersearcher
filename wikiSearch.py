import requests
from selectolax.parser import HTMLParser
from bs4 import BeautifulSoup
import threading
import re
from nltk.corpus import stopwords
from sklearn.feature_extraction.text import TfidfVectorizer as tfidf
import numpy as np
from nltk.tokenize import RegexpTokenizer
import nltk
import torch
from pytorch_pretrained_bert import BertTokenizer, BertForQuestionAnswering, BertModel
import logging
import pickle
from sklearn.metrics.pairwise import cosine_similarity
from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor
from abc import ABC, abstractmethod
import tensorflow as tf

from sifEmbed import SIF
from DAN import Classifier
# from FilterSearcher import FilterSearcher
from FilterSearcher import FilterSearcher
import mediawiki
from mediawiki import MediaWiki


import time

#from preProcessor import PreProcess

class Search():
    def __init__(self, extend):
        self.question = None
        self.regToken = RegexpTokenizer(r'\w+')
        self.article = [] #(url, [list of paragraphs])
        self.query = None
        #self.preProcess = PreProcess()
        self.links = [] #consider removing this
        self.stopwords = []
        self.urlExtend = extend
        self.searcher = FilterSearcher()
        self.getStopwords('stopwords.txt')

    def getStopwords(self, directory):
        with open(directory, 'r') as f:
            lines = f.readlines(1000)
            while lines:
                for line in lines:
                    line = line.replace('\n','')
                    self.stopwords.append(line)
                lines = f.readlines(1000)

    def getval(element):
        return element[1]

    def ask(self, question=None, callback=None, simple=False):
        if(question == None):
            question = self.question

        if(callback is not None):
            self.searcher.getCallBack(callback)
        self.summary = []
        self.article = []
        self.links = []
        self.question = question+'?'

        self.getArticles(self.getLinks(question))
        pickle.dump(self.article, open('articleDump.pckl', 'wb'))
        statement = self.searcher.ask(self.article, question=question, simple=simple)

        return statement

    def retrieveArticle(self, question):
        self.summary = []
        self.article = []
        self.links = []
        self.question = question+'?'
        self.getArticles(self.getLinks(question))



    def askQ(self, question):
        statement = self.searcher.ask(self.article, question=question, simple=False)

        return statement

    def summarize(self, numSentences=5, question=None):
        if(self.searcher.newSummary(question) == True):
            self.question = question + '?'
            self.getArticles(self.getKws())
            self.searcher.giveAbstract(question, self.article)
        self.searcher.summarize(numSentences=numSentences, question=question)
        return self.searcher.summary

    @abstractmethod
    def getLinks(self, searchKw, limit=20):
        pass

    @abstractmethod
    def getArticleText(self, url):
        pass

    def getKws(self):
        words = self.question.lower()
        #words = " ".join([token.lemma_ for token in self.nlp(words)])
        words = self.regToken.tokenize(words)
        words = list(set(words))
        #stopWords = stopwords.words('english')
        searchKws = list(set([word for word in words if word not in self.stopwords]))

        return words

    # def getArticleText(self, url):
    #     resp = requests.get(url)
    #     htmlTxt = resp.text
    #     html = HTMLParser(htmlTxt)
    #     paragraphs = []
    #     for paragraph in html.tags('p'):
    #         text = paragraph.text()
    #         if(len(nltk.word_tokenize(text)) > 5): #According to Chen et al. (DrQA) --> 5 characters < paragraphs < 1500 characters; but I want to make paragraphs limitless
    #             text = re.sub(r'\([^)]*\)','',text) #remove parentheses
    #             text = re.sub(r'\[[^)]*\]','',text)
    #             text = text.replace('\n', '')
    #             paragraphs.append(text)
    #
    #     self.article.append(paragraphs)

    # def getArticles(self, searchKws): #appends articles to links
    #     with ThreadPoolExecutor(max_workers=20) as executor:
    #         res = executor.map(self.getLinks, searchKws)
    #         links = list(res)
    #     for linkBatch in links:
    #         self.links.extend(linkBatch)
    #
    #     print('self links: ', self.links)
    #     urls = [self.urlExtend+extend for extend in self.links]
    #     with ThreadPoolExecutor(max_workers=20) as executor:
    #         executor.map(self.getArticleText, urls)

    def getArticles(self, titles):
        with ThreadPoolExecutor(max_workers=10) as executor:
            res = executor.map(self.getArticleText, titles)
            pages = list(res)

        pages = list(filter(None, pages))

        pages = [page.split('\n') for page in pages]

        pages = list(filter(None, pages))

        #print(pages)

        self.article.extend(pages)



class WikiSearch(Search):
    def __init__(self):
        print('initializing wikisearch')
        Search.__init__(self, "https://en.wikipedia.org")
        self.wikiSearch = MediaWiki()

    def getArticleText(self, url):
        try:
            return self.wikiSearch.page(url).content
        except mediawiki.exceptions.DisambiguationError as e:
            return None

    def getLinks(self, searchKw, limit=10):
        #threadLock.acquire()

        return self.wikiSearch.search(searchKw, results=limit)

        # url = "https://en.wikipedia.org/w/index.php?search="+searchKw+"&title=Special%3ASearch&profile=advanced&fulltext=1&advancedSearch-current={}&ns0=1"
        #
        # resp = requests.get(url)
        # htmlTxt = resp.text
        #
        # #print(limit)
        # soup = BeautifulSoup(htmlTxt, 'lxml')
        # #retrieve links of search query
        # count = 0
        # links = []
        # for a in soup.find_all('div'):
        #     #print(a.attrs.keys())
        #     if('class' in a.attrs.keys()):
        #         if('mw-search-result-heading' in a.attrs['class']):
        #             b = a.find('a')
        #             if(count < limit):
        #                 links.append(b.attrs['href'])
        #                 count += 1
        #             else:
        #                 #print(count)
        #                 #threadLock.release()
        #                 return links
        # return links


# ass = Assistant()
#ass.run()
search = WikiSearch()

print(search.ask("From which countries did the Norse originate"))

# start = time.time()
# print(search.ask("how does a butterfly fly"))
# end = time.time()
# print('total time: ', end-start)
# print(search.summarize())
#
# start = time.time()
# print(search.ask("when was barack obama born"))
# end = time.time()
# print('total time: ', end-start)
#
# start = time.time()
# print(search.ask("what achievements did Socrates make during his lifetime"))
# end = time.time()
# print('total time: ', end-start)

# start = time.time()
# print(search.ask("what did Edison invent"))
# end = time.time()
# print('total time: ', end-start)

# start = time.time()
# print(search.ask("what happened in brexit"))
# end = time.time()
# print('total time: ', end-start)
#
# start = time.time()
# print(search.ask("why did brexit occur"))
# end = time.time()
# print('total time: ', end-start)
#
# start = time.time()
# print(search.ask("what are the repercussions of brexit"))
# end = time.time()
# print('total time: ', end-start)
# topics = pickle.load(open('squad-devTopic.pckl', 'rb'))
#
# with open('answers.txt','w') as f:
#     for topic in topics:
#         for passage in topic:
#             for question in passage[1]:
#                 print(question)
#                 answer = search.ask(question)
#                 print(answer)
#                 f.write('question: '+question+'\n')
#                 f.write('answer: '+answer+'\n\n')

#
# print(search.ask("who is Socrates"))
# # print(search.ask("when was obama born"))
# print(search.ask("what are the applications of genomic testing"))
# print(search.summarize(numSentences=5))
# print(search.ask("what was the name of the dog that went to space"))

#f = open('answers.txt', 'a')
#inputs = open('questions.txt', 'r')
#l = inputs.readlines()
#inputs.close()
#for question in l:
#    f.write(search.ask(question))
#    print('question complete!')
#f.close()

#print(search.ask("who was the brother of Louis XVI?"))
#print(search.ask("How many of Warsaw's inhabitants spoke Polish in 1933?"))
