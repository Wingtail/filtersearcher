import stanfordnlp
from queue import Queue
from sacremoses import MosesDetokenizer
#import spacy

class PreProcess:
    def __init__(self):
        print('initializing stanfordnlp')
        self.nlp = stanfordnlp.Pipeline(processors='tokenize,pos,lemma')
        self.subjects = []
        print('complete')

    def preProcess(self, text):
        doc = self.nlp(text)
        #dependencies = self.getDependencies(doc)
        lemmatized = self.getLemmatized(doc)
        return lemmatized

    def getLemmatized(self, doc):
        document = []
        paragraph = []
        print('lemmatizing')
        k = 0
        i = 0
        document = [token.lemma for token in sentence.words for sentence in doc.sentences]
        document = self.tokensToText(document)
        document = document.split('|||||')
        #paragraph = ' '.join(paragraph)
        print('completed')
        return document

    def tokensToText(self, tokens):
        isCitation = False
        isQuote = False
        isParenthesis = False
        processString = []
        for string in tokens:
            if(string ==']'):
                isCitation = False
            elif(string=='['):
                isCitation = True
            elif(string in ['\'', '\"']):
                isQuote = not isQuote
            elif(string in ['(','{']):
                isParenthesis = True
            elif(string in [')','}']):
                isParenthesis = False
            elif(isCitation==False):
                if(string.find('##')!=-1):
                    string = string.replace('##',"")
                    if(len(processString)>0):
                        lastIndex = len(processString)-1
                        processString[lastIndex] = processString[lastIndex]+string
                elif(string in [',','.',':',';','?','!','...']):
                    if(len(processString)>0):
                        lastIndex = len(processString)-1
                        processString[lastIndex] = processString[lastIndex]+string
                else:
                    processString.append(string)

            if(len(processString)>0):
                if(isQuote == True):
                    lastIndex = len(processString)-1
                    if(processString[lastIndex] in ['\'', '\"']):
                        processString[lastIndex] = processString[lastIndex]+string
                else:
                    lastIndex = len(processString)-1
                    if(string in ['\'', '\"']):
                        processString[lastIndex] = processString[lastIndex]+string

                if(isParenthesis == True):
                    lastIndex = len(processString)-1
                    if(processString[lastIndex] in ['(', '{']):
                        processString[lastIndex] = processString[lastIndex]+string
                else:
                    lastIndex = len(processString)-1
                    if(string in [')', '}']):
                        processString[lastIndex] = processString[lastIndex]+string
        
        statement = ' '.join(processString)
        return statement

    def getDependencies(self, doc):
        self.subjects = []
        print('evaluating dependency')
        dependencyTrees = []
        print('constructing dependency tree')
        for i in range(0,len(doc.sentences)):
            nodes = []
            dependencies = []
            nodes.append(Node('ROOT', 0, "", ""))
            for dependency in doc.sentences[i].dependencies:
                node = Node(dependency[2].text, dependency[2].index, dependency[2].dependency_relation, dependency[2].upos)
                nodes.append(node)

            for dependency in doc.sentences[i].dependencies:
                parent = None
                child = None
                for node in nodes:
                    if(int(node.id)==int(dependency[2].governor)):
                        parent = node
                    if(int(node.id)==int(dependency[2].index)):
                        child = node
                    if(parent != None and child != None):
                        parent.connections.append((child, dependency[2].dependency_relation))
                        break
            q = Queue()
            q.put(nodes[0])
            subject = None
            while(not q.empty()):
                node = q.get_nowait()
                dep = node.dependency
                if(subject == None and dep.find('nsubj') != -1 and node.pos == "NOUN"):
                    subject = node
                for child in node.connections:
                    child[0].rootDist = node.rootDist+1
                    q.put(child[0])
            print(subject)
            dependencyTrees.append(DependencyTree(subject, nodes[0]))
        print('complete')
        return dependencyTrees

class DependencyTree:
    def __init__(self, subject, rootNode):
        if(subject == None):
            self.subject = rootNode.name
        else:
            self.subject = subject.getNameRepresentation()
        self.rootNode = rootNode

class Node:
    def __init__(self, name, id, dependency, pos):
        self.connections = [] #dependency parse
        self.name = name
        self.id = id
        self.pos = pos
        self.dependency = dependency
        self.attributes = []
        self.relationships = []
        self.rootDist = 0

    def indent(self, num):
        for i in range(num-1):
            print('     ', end="")
        print('---- ', end="")
        return

    def printAllConnections(self, attr, count):
        self.indent(count)
        print(self.name, " : ", attr, ' root dist: ', self.rootDist)
        count += 1
        for connection in self.connections:
            connection[0].printAllConnections(connection[1], count)

        return self.connections

    def getNameRepresentation(self):
        string = self.name
        attr = ''
        for connect in self.connections:
            attr += connect[0].getNameRepresentation() + ' '
        string = attr + string
        return string

    def searchDependency(self, dependency):
        que = []
        #from node's connections, search for the dependency
        for connection in self.connections:
            if(connection[1].find(dependency) > -1):
                return connection[0]
        return None

    def searchDependencies(self,dependencies):
        dependencys = []
        for connection in self.connections:
            for dependency in dependencies:
                if(connection[1].find(dependency) > -1):
                    dependencys.append(connection[0])
        return [i for i in dependencys if i != None]


