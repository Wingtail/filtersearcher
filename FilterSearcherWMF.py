import threading
import re
from sklearn.feature_extraction.text import TfidfVectorizer as tfidf
from scipy import spatial
import numpy as np
from nltk.tokenize import RegexpTokenizer
import spacy
import nltk
import torch
from pytorch_pretrained_bert import BertTokenizer, BertForQuestionAnswering, BertModel
import logging
import pickle
import scipy.stats as ss
from sklearn.metrics.pairwise import cosine_similarity
import tensorflow as tf
from nltk.stem.porter import PorterStemmer
from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor

from wordMoverFilter import WMD
from rank_bm25 import BM25Okapi
from mediawiki import MediaWiki
import math

import time

#from preProcessor import PreProcess

class FilterSearcher():
    def __init__(self):
        self.callback = None

        self.models = []
        self.model = None
        self.question = None
        logging.basicConfig(level=logging.INFO)
        self.articles = []
        self.nWorkers = 1
        self.statement = None

        self.tfidf = tfidf(stop_words = None, ngram_range=(1,2))

        self.tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')

        self.summary = []

        self.wmd = WMD()

        graph = tf.get_default_graph()

        for i in range(0,self.nWorkers):
            self.models.append(self.getBertQAModelCopy(i))

    def getCallBack(self, callback):
        self.callback = callback

    def getBertQAModelCopy(self, n): #going to do threading
        with open('BertQA.p', 'rb') as pfile:
            print('creating model ', n)
            model = pickle.load(pfile)
            print('complete')
            return model

    def ask(self, articles, question=None, simple=False):
        if(question == None):
            question = self.question
        self.summary = []
        self.articles = articles
        self.question = question+'?'

        articles = self.retrieveBestArticles(5)
        if(self.callback is not None):
            self.callback()
        paragraphs, simScore = self.retrieveBestParagraphs(articles, 10)
        statement = self.getBestAnswer(paragraphs, simScore, simple=simple)
        return statement

    def evaluate(self, articles, question=None):
        if(question == None):
            question = self.question
        self.summary = []
        self.articles = articles
        self.question = question

        articles = self.retrieveBestArticles(5)
        paragraphs, simScore = self.retrieveBestParagraphs(articles, 10)
        statement = self.getBestAnswer(paragraphs, simScore, simple=True)
        return statement

    def evaluateFromArticles(self, articles, question=None):
        if(question == None):
            question = self.question
        self.summary = []
        self.articles = articles
        self.question = question

        paragraphs, simScore = self.retrieveBestParagraphs(articles, 10)
        statement = self.getBestAnswer(paragraphs, simScore, simple=True)
        return statement

    def evaluateFilter(self, articles, question=None):
        if(question == None):
            question = self.question
        self.summary = []
        self.articles = articles
        self.question = question

        articles = self.retrieveBestArticles(5)
        paragraphs, simScore = self.retrieveBestParagraphs(articles, 20)

        return self.summary

    def retrieveBestArticles(self, k):
        articleFilter = [' '.join(article) for article in self.articles]
        articleFilter.append(self.question)

        tfs = self.tfidf.fit_transform(articleFilter)
        matrix = tfs.todense()

        query = matrix[len(matrix)-1]

        matrix = np.delete(matrix, len(matrix)-1, axis=0)

        sims = np.squeeze(cosine_similarity(query, matrix))

        lookupRank = []

        for i in range(len(articleFilter)-1):
            lookupRank.append((i, sims[i]))

        lookupRank.sort(key=lambda x:x[1], reverse=True) #sort document

        topK = [self.articles[lookupRank[i][0]] for i in range(k)]

        return topK

    def retrieveBestParagraphs(self, articles, k): #Assume paragraph split keyword = <p>
        u = 0.6

        articleText = []

        paragraphs = []

        tot_scores = []

        paragraphs.append(self.question)

        for article in articles:
            for paragraph in article:
                if('==' not in paragraph and len(paragraph) > 30):
                    paragraphs.append(paragraph)

        paragraphs = list(set(paragraphs))

        if(self.callback is not None):
            self.callback()

        distances = self.wmd.getRWMD(paragraphs)

        # print('distances: ', distances)

        weights = 1.0 - (distances-np.nanmin(distances))/(np.nanmax(distances)-np.nanmin(distances))

        del paragraphs[0]

        mapping = [(i, weights[i]) for i in range(len(distances))]
        # mapping = [pair for pair in mapping if math.isnan(pair[1])==False]
        mapping.sort(key=lambda x:x[1], reverse=False)

        mapping = mapping[:k]

        topK = [paragraphs[pair[0]] for pair in mapping]

        similarityScore = np.zeros(k)

        for i in range(k):
            similarityScore[i] = mapping[i][1]

        #general context filter with cosine similarity

        ##linguistic filter --> Okapi BM25

        #print('topK: ', topK)

        return (topK, similarityScore)

    def giveAbstract(self, question, articles, numSentences=5):
        self.articles = articles
        self.question = question+'?'
        articles = self.retrieveBestArticles(5)
        paragraphs = self.retrieveBestParagraphs(articles, 10)
        #return self.summarize(numSentences=numSentences)
        # answers, _ = self.getBestAnswer(paragraphs)
        #
        # answer = [phrase for phrase in answers if (len(phrase)>0)]
        # ans = []
        # for a in answer:
        #     ans.extend(a)
        #     ans.append('.')
        #
        # paragraph = self.tokensToText(ans)
        # print('final answers: ',paragraph)

    def newSummary(self, question):
        if(question is not None and self.question is not None):
            embeds = self.sif.sifEmbed([self.question, question+'?'])
            if(cosine_similarity(embeds)[0][1] <= 0.6):
                return True
        elif(self.question is None):
            return True

        return False
    def summarize(self, numSentences=1, question=None):
        sentences = []

        for paragraph in self.summary:
            sentences.extend(nltk.sent_tokenize(paragraph))

        if(self.statement is not None):
            print(self.statement)
            sentences.append(self.statement)

            summary = []

            sentenceLimit = min(numSentences, len(sentences))
            embeddings = self.getSentEncoding(sentences)

            statement = np.expand_dims(embeddings[len(embeddings)-1], axis=0)
            index = len(embeddings)-1
            # else:
            #     sims = cosine_similarity(embeddings)
            #     avgSims = np.sum(sims, axis=1) / len(sims)
            #     index = np.argmax(avgSims)
            #     statement = np.expand_dims(embeddings[index], axis=0)
            #     summary.append(sentences[index])

            for _ in range(sentenceLimit):
                embeddings = np.delete(embeddings, index, 0)
                del sentences[index]
                sims = cosine_similarity(statement, embeddings)
                index = np.argmax(sims[0])
                summary.append(sentences[index])
                statement = np.expand_dims(embeddings[index], axis=0)

            self.summary = ' '.join(summary)
        else:
            self.summary = ' '.join(sentences[:numSentences])

    def vectorizeDocument(self, documentRepresentation):
        documents = [] #(link, tokens, vectorList)
        for doc in documentRepresentation:
            text = self.article[doc]
            print('TEXT: ',text)
            tokens = ['[CLS]'].extend(self.tokenizer.tokenize(text))
            tokens.append('[SEP]')
            segmentId = [0 for i in range(0,len(tokens))]
            periodIndices = [i for i in range(0,len(tokens)) if tokens[i] in ['.', '?', '!', ';']]

            splice = []
            if(len(tokens) > 512):
                n = 0
                val = 0
                for i in periodIndices:
                    if(i-(512*n) <= 512):
                        val = i
                    else:
                        splice.append(val)
                        n += 1
                        val = i
            tokenIds = []
            for i in range(0,len(splice)-1):
                tokenId = self.tokenizer.convert_tokens_to_ids(tokens[splice[i]+1:splice[i+1]])
                tokenIds.append(tokenId)
            #start segment
            tokenIds.append(self.tokenizer.convert_tokens_to_ids(tokens[0,splice[0]]))
            #end segment
            if(splice[len(splice)-1] < len(tokens)-1):
                tokenIds.append(self.tokenizer.convert_tokens_to_ids(tokens[tokens[splice[len(splice)-1]]:len(tokens)]))

            for tokenId in tokenIds:
                tokens_tensor = torch.tensor([tokenId])
                segments_tensors = torch.tensor([[0 for i in range(0,len(tokenId))]])
                with torch.no_grad():
                    encoded_layers, _ = self.bertRaw(tokens_tensor, segments_tensors)
        return

    def tokenizeParagraph(self, tokenQuestion, paragraph, tokenized=False):
        if(tokenized == False):
            paragraph = self.tokenizer.tokenize(paragraph)
        tokens = []
        tokens.extend(['[CLS]'])
        tokens.extend(tokenQuestion)
        tokens.extend(['[SEP]'])
        tokens.extend(paragraph)
        tokens.extend(['[SEP]'])

        #print(tokens)

        i=0 #Clean the fucking code
        segments = []
        while (tokens[i] != '[SEP]'):
            segments.append(0)
            i+=1
        segments.append(0)
        i+=1
        count = 0
        while (tokens[i] != '[SEP]'):
            segments.append(1)
            count+=1
            i+=1
        segments.append(1)
        i+=1

        tokenId = self.tokenizer.convert_tokens_to_ids(tokens)
        tokensTensor = torch.tensor([tokenId])
        segmentTensor = torch.tensor([segments])
        return (tokensTensor, segmentTensor, tokenId)

    def answer(self, tup, model=None, getSimple=False): #parameters in token form
        tokensTensor = tup[0]
        segmentTensor = tup[1]
        print('starting')
        if(model==None):
            with torch.no_grad():
                prediction = self.model(tokensTensor, segmentTensor)
        else:
            with torch.no_grad():
                prediction = model(tokensTensor, segmentTensor)

        print('complete')
        if(self.callback is not None):
            self.callback()
        return prediction

    def autoTokenize(self, text):
        batch = []
        puncs = ['.','?','!','[SEP]','[CLS]']


        tokenQuestion = self.tokenizer.tokenize(self.question)
        questionLength = len(tokenQuestion)

        filling = questionLength + 3
        words = self.tokenizer.tokenize(text)

        indices = [i for i in range(len(words)) if words[i] in puncs]

        start = 0
        end = 0
        for index in indices:
            if(end-start < 512-filling and (end+index+1)-start < 512-filling):
                end = index+1
            else:
                batch.append(words[start:end])
                start = end

        if(start != end):
            batch.append(words[start:end])

        return batch

    def getBestAnswer(self, documentRepresentation, simScore, simple=False):
        answers = []
        batches = []
        tokenQuestion = self.tokenizer.tokenize(self.question)
        #qSize = len(tokenQuestion)

        numBatches = []

        for i in range(0,len(documentRepresentation)):
            #check if paragraph is too long
            text = documentRepresentation[i]
            #text = ' '.join(text)
            #words = self.tokenizer.tokenize(text) #tokenizing article

            b = self.autoTokenize(text)

            numBatches.append(len(b))

            batches.extend(b)

        if(self.callback is not None):
            self.callback()

        #executing multiprocessing
        answers = []
        for batch in batches:
            tokens = self.tokenizeParagraph(tokenQuestion, batch, tokenized=True)
            answers.append(tokens)

        if(self.callback is not None):
            self.callback()

        processors = [self.models[i%len(self.models)] for i in range(0,len(answers))]
        #print(processors)
        predictions = []

        with ThreadPoolExecutor(max_workers=len(self.models)) as executor:
            res = executor.map(self.answer, answers, processors)

        result = list(res)
        predictions = result

        candidates = []


        answerCount = 0
        count = 0

        index = 0
        totScores = []

        for prediction in predictions:
            tokenId = [i.item() for i in answers[answerCount][0][0]]
            answer, score = self.segToSentence(tokenId, prediction, tokenQuestion, getSimple=simple)
            candidates.append(answer)
            totScores.append(simScore[index] * score)

            count +=1
            answerCount += 1
            if(count == numBatches[index]):
                index += 1
                count = 0

        arg = np.argmax(np.asarray(totScores))

        bestAnswer = candidates[arg]

        bestAnswer = self.tokensToText(bestAnswer)
        self.statement = bestAnswer
        self.summary = candidates

        if(self.callback is not None):
            self.callback()

        print(self.summary)

        if(len(bestAnswer) <=0 ):
            return ''.join(self.summary[0])

        # print('candidates: ', candidates)

        print(bestAnswer)

        return bestAnswer

    def segToSentence(self, tokenId, prediction, tokenQuestion, getSimple=False):
            score = (torch.max(prediction[0]).item() + torch.max(prediction[1]).item()) / 2.0
            tokenId = self.tokenizer.convert_ids_to_tokens(tokenId)
            #print(tokenId)
            stopPuncs = ['.','?','!','[SEP]','[CLS]']
            startIndex = torch.argmax(prediction[0]).item()
            endIndex = torch.argmax(prediction[1]).item()
            #print(startIndex,' ', endIndex)
            answer = []
            if(getSimple==False):
                if(startIndex <= len(tokenQuestion) or endIndex <= len(tokenQuestion)):
                    answer = []
                else:
                    while(endIndex < len(tokenId) and tokenId[endIndex] not in stopPuncs):
                        endIndex+=1
                    while(startIndex > 0 and tokenId[startIndex] not in stopPuncs):
                        startIndex-=1
                    answer = [tokenId[i] for i in range(startIndex+1, endIndex)]
            else:
                if(startIndex > 0 and endIndex < len(tokenId)-1):
                    if(startIndex == endIndex):
                        answer = [tokenId[startIndex]]
                    else:
                        answer = [tokenId[i] for i in range(startIndex, endIndex+1)]
                else:
                    answer = []

            # answer = [tokenId[i] for i in range(startIndex, endIndex)]

            return (answer, score)

    def tokensToText(self, tokens):
        isCitation = False
        isQuote = False
        isParenthesis = False
        processString = []
        for string in tokens:
            if(string ==']'):
                isCitation = False
            elif(string=='['):
                isCitation = True
            elif(string in ['\'', '\"']):
                isQuote = not isQuote
            elif(string in ['(','{']):
                isParenthesis = True
            elif(string in [')','}']):
                isParenthesis = False
            elif(isCitation==False):
                if(string.find('##')!=-1):
                    string = string.replace('##',"")
                    if(len(processString)>0):
                        lastIndex = len(processString)-1
                        processString[lastIndex] = processString[lastIndex]+string
                elif(string in [',','.',':',';','?','!','...']):
                    if(len(processString)>0):
                        lastIndex = len(processString)-1
                        processString[lastIndex] = processString[lastIndex]+string
                else:
                    processString.append(string)

            if(len(processString)>0):
                # if(isQuote == True):
                #     lastIndex = len(processString)-1
                #     if(processString[lastIndex] in ['\'', '\"']):
                #         processString[lastIndex] = processString[lastIndex]+string
                # else:
                #     lastIndex = len(processString)-1
                #     if(string in ['\'', '\"']):
                #         processString[lastIndex] = processString[lastIndex]+string

                if(isParenthesis == True):
                    lastIndex = len(processString)-1
                    if(processString[lastIndex] in ['(', '{']):
                        processString[lastIndex] = processString[lastIndex]+string
                else:
                    lastIndex = len(processString)-1
                    if(string in [')', '}']):
                        processString[lastIndex] = processString[lastIndex]+string

        statement = ' '.join(processString)
        return statement

    def getFinalAnswer(self, answers, tokenQuestion, simScore, bertScore, simple=False): #returns in full text format
        answer = [phrase for phrase in answers if (len(phrase)>0)]

        finalScore = simScore * bertScore

        print('final score: ', finalScore)


        # ans = []
        # for a in answer:
        #     ans.extend(a)
        #     ans.append('.')
        #
        # #print(ans)
        #
        # paragraph = self.tokensToText(ans)
        # #print('final answers: ',paragraph)
        #
        # count = 0
        # statement = ''
        # if(len(answer)>1): #paragraph
        #     tokens = self.tokenizeParagraph(tokenQuestion, ans, tokenized = True)
        #     prediction = self.answer(tokens, self.models[0])
        #     tokenId = [i.item() for i in tokens[0][0]]
        #
        #     answer = self.segToSentence(tokenId, prediction, tokenQuestion, getSimple = simple)
        #     #print('ANSWER: ', answer)
        #
        # else:
        #     return paragraph
        # statement = self.tokensToText(answer)
        # print('summaries: ',self.summary)
        #
        # if(len(statement) <= 0):
        #     return ' '.join(self.summary[0])

        self.statement = statement

        return statement
