import tensorflow as tf

class LayerNormalization(tf.layers.Layer):
    def __init__(self, hidden_size):
        super(LayerNormalization, self).__init__()
        self.hidden_size = hidden_size
        self.scale = tf.Variable(tf.ones([self.hidden_size]), trainable=True)
        self.bias = tf.Variable(tf.ones([self.hidden_size]), trainable=True)

    def call(self, x, epsilon=1e-6):
        mean = tf.reduce_mean(x, axis=[-1], keepdims=True)
        variance = tf.reduce_mean(tf.square(x - mean), axis=[-1], keepdims=True)
        norm_x = (x - mean) * tf.rsqrt(variance + epsilon)
        return norm_x * self.scale + self.bias

class Embedding(tf.layers.Layer):
    def __init__(self, wordEmbed):
        super(Embedding,self).__init__()
        self.params = wordEmbed

    def call(self, x):
        return tf.nn.embedding_lookup(self.params, x)

class DAN(object):
    def __init__(self, wordEmbed):
        self.layers = []
        self.embed = Embedding(wordEmbed)
        for _ in range(4):
            self.layers.append(tf.layers.Dense(512, activation=tf.nn.elu))

    def call(self, x, training=False):
        # x = tf.cast(self.embed(x), tf.float32)
        avg = tf.reduce_sum(x, axis=1)
        avg /= tf.reduce_sum(tf.cast(tf.math.not_equal(tf.reduce_sum(tf.math.abs(x), axis=2, keepdims=True), 0), tf.float32), axis=1)
        for i in range(len(self.layers)):
            layer = self.layers[i]
            avg = layer(avg)

        return avg

class Classifier(object):
    def __init__(self):
        # self.dense3 = tf.layers.Dense(512, activation=tf.nn.relu, kernel_regularizer=tf.contrib.layers.l2_regularizer(0.001))
        # self.dense4 = tf.layers.Dense(256, activation=tf.nn.relu, kernel_regularizer=tf.contrib.layers.l2_regularizer(0.001))
        # self.dense5 = tf.layers.Dense(128, activation=tf.nn.relu, kernel_regularizer=tf.contrib.layers.l2_regularizer(0.001))
        # self.dense6 = tf.layers.Dense(64, activation=tf.nn.sigmoid, kernel_regularizer=tf.contrib.layers.l2_regularizer(0.001))
        # self.dense7 = tf.layers.Dense(1, activation=tf.nn.sigmoid)

        # self.dense1 = tf.layers.Dense(1600, activation=tf.nn.relu)
        # self.dense2 = tf.layers.Dense(1024, activation=tf.nn.relu)
        self.layers = []
        for _ in range(3):
            self.layers.append(tf.layers.Dense(512, activation=tf.nn.elu))
        # self.dense5 = tf.layers.Dense(512, activation=tf.nn.elu)
        # self.dense6 = tf.layers.Dense(512, activation=tf.nn.elu)
        self.sig = tf.layers.Dense(1, activation=tf.nn.sigmoid)
        # self.lnorm1 = LayerNormalization(512)
        # self.lnorm2 = LayerNormalization(512)
        # self.bnorm = tf.layers.BatchNormalization()


    def call(self, x1, x2, training=False):
        multiplied = tf.math.multiply(x1, x2)
        difference = tf.math.abs(tf.math.subtract(x1, x2))
        concat = tf.concat([x1, x2, multiplied, difference], 1)

        # out = self.dense1(concat)
        # out = self.dense2(out)
        # if(training == True):
        #     #concat = self.bnorm(concat)
        #     concat = tf.nn.dropout(concat, rate=0.1)

        for i in range(len(self.layers)):
            layer = self.layers[i]
            concat = layer(concat)

        out = self.sig(concat)

        return out
