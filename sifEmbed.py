import numpy as np
from sklearn.decomposition import TruncatedSVD, SparsePCA
from scipy.special import softmax
import sif_utils as utils
import nltk
from tqdm import tqdm
import unkModel
from guppy import hpy
from sklearn.metrics.pairwise import cosine_similarity

class SIF:
    def __init__(self, params):
        self.vocabDict = params['vocabulary']
        self.vecLookup = params['vecLookup']
        self.freqLookup = params['freqLookup']
        self.dim = params['dim']
        self.a = params['alpha']
        self.params = params
        self.unkModel = unkModel.UnkModel(params)

    def computePrimaryComponent(self, embedding):
        svd = TruncatedSVD(n_components=1, n_iter=7, random_state=0)
        svd.fit(embedding)
        return svd.components_

    def removePrimaryComponent(self, embedding):
        pc = self.computePrimaryComponent(embedding)

        projection = embedding.dot(pc.transpose()).dot(pc)

        embedding = embedding - projection

        return embedding

    def getVal(self, dic, key):
        if(key in dic):
            return dic.__getitem__(key)

    def getWordList(self, sentence):
        return nltk.word_tokenize(sentence.lower())

    def getValues(self, sentences):
        maxLength = 0
        wordList = []
        words = []
        posList = []
        vocabDict = self.vocabDict.copy()
        vecLookup = self.vecLookup
        freqLookup = self.freqLookup

        wordList = list(map(self.getWordList, sentences))

        print('wordList len: ',len(wordList))

        maxes = list(map(lambda x:len(x), wordList))
        maxLength = max(maxes)

        for wordlist in wordList:
            words.extend(wordlist)

        print('words len: ', len(words))

        print('got wordList')
        output = self.unkModel.getUnkWords(words)

        if(len(output[0]) > 0):
            vecLookup = np.concatenate((self.vecLookup, output[0]), axis=0)
            freqLookup = np.concatenate((self.freqLookup, output[1]), axis=0)

            ID = len(list(vocabDict.keys()))
            for key in output[2]:
                vocabDict[key] = ID
                ID += 1

        print('got unks')

        print('vecLookup len: ', vecLookup.shape)
        print('freqLookup len: ', freqLookup.shape)

        print('appended unks')

        sentenceIndex = np.zeros((len(sentences), maxLength), dtype=int)

        print('sentIndex: ', sentenceIndex.shape)

        indices = list(map(lambda x: vocabDict.__getitem__(x) if x in vocabDict and len(x) > 0 else 0, words))

        index = 0
        for i in range(len(maxes)):
            length = maxes[i]
            if(length > 0):
                sentenceIndex[i][0:length] = indices[index:index+length]
                index += length

        print('got sentenceIndices')

        vectors = vecLookup[sentenceIndex.ravel()]
        vectors = vectors.reshape((len(sentences), maxLength, 300))

        print('word vectors shape: ', vectors.shape)

        print('got vectors')

        quotient = np.expand_dims(np.count_nonzero(sentenceIndex, axis=1), axis=1)

        quotient = np.where(quotient == 0, 1.0, quotient) #prevent dividing by 0

        print('got quotient')

        freqs = freqLookup[sentenceIndex].transpose((0,2,1))

        print('freq: ', freqs)
        print('freq shape: ', freqs.shape)


        freqs = self.a / (self.a+freqs)

        print('got frequency')

        quotient = np.expand_dims(quotient, axis=1)

        return (vectors, freqs, quotient)

    def sifEmbed(self, sentences, removeComponent=True):
        values = self.getValues(sentences)

        embedding = np.matmul(values[1],values[0])

        print('got embedding')

        embedding /= values[2]


        sentenceVector = np.squeeze(embedding)

        print('squeezed')

        if(removeComponent==True and len(sentenceVector) > 2):
            sentenceVector = self.removePrimaryComponent(sentenceVector)
            print('removed principal component')

        return (sentenceVector)

    def sifEmbedParagraphs(self, paragraphs):
        #Assume that paragraphs is 2-D array --> [[sentences]] each sentence is element
        #Weighted sentence edition

        sentenceBatch = ['']
        paragraphIndices = [] #each element is numpy format

        avgWeights = []

        totSentences = list(map(lambda paragraph:len(paragraph), paragraphs))
        maxLength = max(totSentences)

        paragraphIndices = np.zeros((len(paragraphs), maxLength), dtype=int)
        ID = 1

        with tqdm(total=len(paragraphs)) as pbar:
            for i in range(len(paragraphs)):
                indices = list(range(ID, ID+len(paragraphs[i])))
                paragraphIndices[i][:len(indices)] = indices
                ID += len(paragraphs[i])
                sentenceBatch.extend(paragraphs[i])
                pbar.update(1)

        print('loaded paragraphIndices')

        print('sentenceBatch length: ', len(sentenceBatch))

        sentenceVectors = self.sifEmbed(sentenceBatch) # (totSentences, dimension)

        print('embedded sentences')

        paragraphs = sentenceVectors[paragraphIndices]


        quotient = np.expand_dims(np.count_nonzero(paragraphIndices, axis=1), axis=1)

        print('quotient: ', quotient.shape)

        quotient = np.where(quotient == 0, 1.0, quotient)

        paragraphVectors = np.sum(paragraphs, axis=1)


        paragraphVectors /= quotient

        return paragraphVectors

    def sifDistance(self, paragraphs):
        sentenceBatch = ['']
        paragraphIndices = [] #each element is numpy format

        avgWeights = []

        totSentences = list(map(lambda paragraph:len(paragraph), paragraphs))
        maxLength = max(totSentences)

        paragraphIndices = np.zeros((len(paragraphs), maxLength), dtype=int)
        ID = 1

        with tqdm(total=len(paragraphs)) as pbar:
            for i in range(len(paragraphs)):
                indices = list(range(ID, ID+len(paragraphs[i])))
                paragraphIndices[i][:len(indices)] = indices
                ID += len(paragraphs[i])
                sentenceBatch.extend(paragraphs[i])
                pbar.update(1)

        print('loaded paragraphIndices')

        print('sentenceBatch length: ', len(sentenceBatch))

        sentenceVectors = self.sifEmbed(sentenceBatch) # (totSentences, dimension)
        query = np.expand_dims(sentenceVectors[len(sentenceVectors)-1], axis=0)

        # sentenceVectors = np.delete(sentenceVectors, len(sentenceVectors)-1, axis=0)
        sims = np.transpose(cosine_similarity(query, sentenceVectors), (1,0))

        sims[sims == 0.0] = np.nan

        sims = sims[paragraphIndices]

        means = np.nanmean(sims, axis=1)

        return np.squeeze(means)
