import chars2vec
from collections import defaultdict
import numpy as np
import tensorflow as tf

class UnkModel():
    def __init__(self, params=None):
        self.charModel = chars2vec.load_model('eng_300')
        self.graph = tf.get_default_graph()
        self.params = params

    def getUnkWords(self, document): #[word, word] --> array with words in document
        #Pre-processing
        if(self.params is not None):
            with self.graph.as_default():
                vocabDict = self.params['vocabulary']
                totWords = float(len(document))
                count = defaultdict(int)

                for word in document:
                    count[word] += 1

                unks = list(filter(lambda word:word not in vocabDict, document))

                unkFreq = np.expand_dims(np.array(list(map(lambda word: (count[word]/totWords), unks))), axis=1)
                unkVec = self.charModel.vectorize_words(unks)

                return (unkVec, unkFreq, unks)

    def getUnkVector(self, unks):
        return self.charModel.vectorize_words(unks)
