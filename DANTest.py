import tensorflow as tf

from sifEmbed import SIF
from DAN import Classifier
from sklearn.metrics.pairwise import cosine_similarity
import pickle
import numpy as np

vocabDict = pickle.load(open('sif_vocabDict.pckl', "rb"))
vecLookup = pickle.load(open('sif_vecLookup.pckl', "rb"))
freqLookup = pickle.load(open('sif_freqLookup.pckl', "rb"))

params = {
'alpha': 1e-3,
'dim':300,
'vocabulary': vocabDict,
'vecLookup':vecLookup,
'freqLookup':freqLookup,
}

s1 = tf.placeholder(tf.float32, shape=(None, 300))
s2 = tf.placeholder(tf.float32, shape=(None, 300))

model = Classifier()
output = model.call(s1,s2)

saver = tf.train.Saver()
session = tf.Session()
saver.restore(session, "./paragraphFilter_gen2/")
print('ParagraphFilter loaded')

sif = SIF(params)

sentences = ["Who is the first president of Korea?", "Il Song Hyun, the 1st president of South Korea, brought wealth to our country",
"I am a sentence for which I would like to get its embedding", "I hate you."]

articleEmbedding = sif.sifEmbedParagraphs(sentences)
question = np.expand_dims(articleEmbedding[1], axis=0)

articleEmbedding = np.delete(articleEmbedding, len(sentences)-1, axis=0)

question = np.repeat(question, len(articleEmbedding), axis=0)

logits = session.run(output, feed_dict={s1:articleEmbedding, s2:question})

print(logits)
