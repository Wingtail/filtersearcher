from googlesearch import search
from FilterSearcher import FilterSearcher
from nltk.tokenize import RegexpTokenizer
from html2text import html2text
from abc import ABC, abstractmethod
import requests
from selectolax.parser import HTMLParser
import nltk
import re
from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

class Search():
    def __init__(self):
        self.question = None
        self.regToken = RegexpTokenizer(r'\w+')
        self.article = [] #(url, [list of paragraphs])
        self.query = None
        #self.preProcess = PreProcess()
        self.links = [] #consider removing this
        self.searcher = FilterSearcher()

    def getStopwords(self, directory):
        with open(directory, 'r') as f:
            lines = f.readlines(1000)
            while lines:
                for line in lines:
                    line = line.replace('\n','')
                    self.stopwords.append(line)
                lines = f.readlines(1000)

    def getval(element):
        return element[1]

    def ask(self, question=None, callback=None, simple=False):
        if(question == None):
            question = self.question

        if(callback is not None):
            self.searcher.getCallBack(callback)
        self.summary = []
        self.article = []
        self.links = []
        self.question = question+'?'

        self.getArticles(self.getLinks(question))
        statement = self.searcher.ask(self.article, question=question, simple=simple)

        return statement

    def retrieveArticle(self, question):
        self.summary = []
        self.article = []
        self.links = []
        self.question = question+'?'
        self.getArticles(self.getLinks(question))

    def askQ(self, question):
        statement = self.searcher.ask(self.article, question=question, simple=False)

        return statement

    def summarize(self, numSentences=5, question=None):
        if(self.searcher.newSummary(question) == True):
            self.question = question + '?'
            self.getArticles(self.getKws())
            self.searcher.giveAbstract(question, self.article)
        self.searcher.summarize(numSentences=numSentences, question=question)
        return self.searcher.summary

    @abstractmethod
    def getLinks(self, searchKw, limit=20):
        pass

    @abstractmethod
    def getArticleText(self, url):
        pass

    def getKws(self):
        words = self.question.lower()
        #words = " ".join([token.lemma_ for token in self.nlp(words)])
        words = self.regToken.tokenize(words)
        words = list(set(words))
        #stopWords = stopwords.words('english')
        searchKws = list(set([word for word in words if word not in self.stopwords]))

        return words

    # def getArticleText(self, url):
    #     resp = requests.get(url)
    #     htmlTxt = resp.text
    #     html = HTMLParser(htmlTxt)
    #     paragraphs = []
    #     for paragraph in html.tags('p'):
    #         text = paragraph.text()
    #         if(len(nltk.word_tokenize(text)) > 5): #According to Chen et al. (DrQA) --> 5 characters < paragraphs < 1500 characters; but I want to make paragraphs limitless
    #             text = re.sub(r'\([^)]*\)','',text) #remove parentheses
    #             text = re.sub(r'\[[^)]*\]','',text)
    #             text = text.replace('\n', '')
    #             paragraphs.append(text)
    #
    #     self.article.append(paragraphs)

    # def getArticles(self, searchKws): #appends articles to links
    #     with ThreadPoolExecutor(max_workers=20) as executor:
    #         res = executor.map(self.getLinks, searchKws)
    #         links = list(res)
    #     for linkBatch in links:
    #         self.links.extend(linkBatch)
    #
    #     print('self links: ', self.links)
    #     urls = [self.urlExtend+extend for extend in self.links]
    #     with ThreadPoolExecutor(max_workers=20) as executor:
    #         executor.map(self.getArticleText, urls)

    def getArticles(self, titles):
        with ThreadPoolExecutor(max_workers=10) as executor:
            res = executor.map(self.getArticleText, titles)
            pages = list(res)

        pages = list(filter(None, pages))

        pages = [page.split('\n') for page in pages]

        pages = list(filter(None, pages))

        #print(pages)

        self.article.extend(pages)



class WikiSearch(Search):
    def __init__(self):
        print('initializing wikisearch')
        Search.__init__(self)

    def getArticleText(self, url):
        resp = requests.get(url, verify=False)
        htmlTxt = resp.text
        html = HTMLParser(htmlTxt)
        paragraphs = []
        for paragraph in html.tags('p'):
            text = paragraph.text()
            if(len(text) > 5): #According to Chen et al. (DrQA) --> 5 characters < paragraphs < 1500 characters; but I want to make paragraphs limitless
                # text = re.sub(r'\([^)]*\)','',text) #remove parentheses
                text = re.sub(r'\[[^)]*\]','',text)
                text = text.replace('\n', '')
                paragraphs.append(text)

        print(url,' complete')
        self.article.append(paragraphs)

    def getLinks(self, searchKw, limit=10):
        return search(searchKw, stop=10)

searcher = WikiSearch()

# print(searcher.ask("Who is the first president of the United States"))
print(searcher.ask("when was barack obama born"))
# print(searcher.ask("who is einstein"))
