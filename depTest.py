from preProcessor import PreProcess
from queue import Queue
from sklearn.feature_extraction import stop_words
dep = PreProcess()

deps = dep.preProcess("Genes encode the information needed by cells for the synthesis of proteins, which in turn play a central role in influencing the final phenotype of the organism. Genetics provides research tools used in the investigation of the function of a particular gene, or the analysis of genetic interactions. Within organisms, genetic information is physically represented as chromosomes, within which it is represented by a particular sequence of amino acids in particular DNA molecules.")

deps = dep.preProcess("Physiology is the study of the mechanical, physical, and biochemical processes of living organisms function as a whole. The theme of \"structure to function\" is central to biology. Physiological studies have traditionally been divided into plant physiology and animal physiology, but some principles of physiology are universal, no matter what particular organism is being studied. For example, what is learned about the physiology of yeast cells can also apply to human cells. The field of animal physiology extends the tools and methods of human physiology to non-human species. Plant physiology borrows techniques from both research fields.")


print(deps[0])
print(deps[0][1].subject)
deps[0][1].rootNode.printAllConnections('',0)
print(deps[3][1].subject)
deps = dep.preProcess("What is learned abouot the physiology of yeast cells?")
rootNode = deps[0][1].rootNode
q = Queue()
q.put(rootNode)
stopWords = stop_words.ENGLISH_STOP_WORDS
while(not q.empty()):
    node = q.get()
    print(node)
    connections = []
    for connection in node.connections:
        if(connection[0].dependency not in ['case', 'det', 'punct'] and connection[0].name not in stopWords):
            connections.append(connection)
            q.put(connection[0])
    node.connections = connections
deps[0][1].rootNode.printAllConnections('',0)
print(deps[0][1].subject)
