import numpy as np
from sklearn.decomposition import TruncatedSVD, SparsePCA
from scipy.special import softmax
import sif_utils as utils
import nltk
from tqdm import tqdm
import chars2vec

class Model():
    def __init__(self):
        self.charModel = chars2vec.load_model('eng_300')

class SIF:
    def __init__(self, params):
        self.model = Model()
        self.vocabDict = params['vocabulary']
        self.vecLookup = params['vecLookup']
        self.freqLookup = params['freqLookup']
        self.dim = params['dim']
        self.a = params['alpha']

    def computePrimaryComponent(self, embedding):
        svd = TruncatedSVD(n_components=1, n_iter=7, random_state=0)
        #svd = SparsePCA(n_components=1)
        svd.fit(embedding)
        return svd.components_

    def removePrimaryComponent(self, embedding):
        pc = self.computePrimaryComponent(embedding)

        projection = embedding.dot(pc.transpose()).dot(pc)

        embedding = embedding - projection

        return embedding

    def getVal(self, dic, key):
        if(key in dic):
            return dic.__getitem__(key)

    def getWordList(self, sentence):
        return nltk.word_tokenize(sentence.lower())

    def fillUnk(self, wordList):
        unks = {}

        ID = len(list(self.vocabDict.keys()))
        for words in wordList:
            for word in words:
                if(word not in self.vocabDict):
                    unks[word] = 3e-7
                    self.vocabDict[word] = ID
                    ID += 1

        unkWords = list(unks.keys())
        unkFreq = np.asarray(list(map(lambda word:unks[word], unkWords)))
        unkFreq = np.reshape(unkFreq, (len(unkWords), 1))

        unkEmbeddings = self.model.charModel.vectorize_words(unkWords)

        return (unkFreq, unkEmbeddings)


    def getValues(self, sentences):
        maxLength = 0
        wordList = []
        posList = []

        #print('sentences',sentences)
        # stopwords = params['stopwords']

        wordList = list(map(self.getWordList, sentences))

        # for i in range(len(wordList)):
        #     wordList[i] = [word for word in wordList[i] if word not in stopwords]

        maxes = list(map(lambda x:len(x), wordList))
        maxLength = max(maxes)

        print('got wordList')
        unks = self.fillUnk(wordList)
        self.freqLookup = np.append(self.freqLookup, unks[0], axis=0)
        self.vecLookup = np.append(self.vecLookup, unks[1], axis=0)

        print('appended unks')

        sentenceIndex = np.zeros((len(sentences), maxLength), dtype=int)

        with tqdm(total=len(wordList)) as pbar:
            for i in range(len(wordList)):
                indices = list(map(lambda x: self.vocabDict.__getitem__(x) if x in self.vocabDict else 0, wordList[i]))
                sentenceIndex[i][0:len(indices)] = indices
                pbar.update(1)

        print('got sentenceIndices')

        vectors = self.vecLookup[sentenceIndex]

        #print('vectors: ', vectors)

        print('got vectors')

        quotient = np.expand_dims(np.count_nonzero(sentenceIndex, axis=1), axis=1)

        quotient = np.where(quotient == 0, 1.0, quotient) #prevent dividing by 0

        print('got quotient')

        freqs = self.freqLookup[sentenceIndex].transpose((0,2,1))

        #print('freq: ',freqs)

        filter = (freqs != 1e9).astype(float)

        avgWeights = freqs * filter

        #print('avgWeights: ', avgWeights)

        avgWeights = self.a / (self.a + (np.sum(avgWeights, axis=2) / quotient))

        #print('avgWeights: ',avgWeights)

        freqs = self.a / (self.a+freqs)

        print('got frequency')

        quotient = np.expand_dims(quotient, axis=1)

        print('got avgWeights')

        return (vectors, freqs, quotient, avgWeights)

    def sifEmbed(self, sentences):
        values = self.getValues(sentences)

        embedding = np.matmul(values[1],values[0])

        print('got embedding')

        embedding /= values[2]

        #print('embedding: ', embedding)

        sentenceVector = np.squeeze(embedding, axis=1)

        print('squeezed')

        sentenceVector = self.removePrimaryComponent(sentenceVector)

        print('removed principal component')

        #print('sentence vector: ', sentenceVector)

        return (sentenceVector, values[3])

    def sifEmbedParagraphs(self, paragraphs):
        #Assume that paragraphs is 2-D array --> [[sentences]] each sentence is element
        #Weighted sentence edition

        sentenceBatch = ['']
        paragraphIndices = [] #each element is numpy format

        avgWeights = []

        totSentences = list(map(lambda paragraph:len(paragraph), paragraphs))
        maxLength = max(totSentences)

        paragraphIndices = np.zeros((len(paragraphs), maxLength), dtype=int)
        ID = 1

        with tqdm(total=len(paragraphs)) as pbar:
            for i in range(len(paragraphs)):
                indices = list(range(ID, ID+len(paragraphs[i])))
                paragraphIndices[i][:len(indices)] = indices
                ID += len(paragraphs[i])
                sentenceBatch.extend(paragraphs[i])
                pbar.update(1)

        print('loaded paragraphIndices')

        sentenceVectors = self.sifEmbed(sentenceBatch) # (totSentences, dimension)

        print('embedded sentences')
        # print(sentenceVectors)
        # print(paragraphIndices)

        paragraphs = sentenceVectors[0][paragraphIndices]
        weights = sentenceVectors[1][paragraphIndices]

        #print('paragraphVectors: ', paragraphs)

        quotient = np.expand_dims(np.count_nonzero(paragraphIndices, axis=1), axis=1)

        #print('quotient: ', quotient)

        quotient = np.where(quotient == 0, 1.0, quotient)
        # print(paragraphs)
        #print('weights: ',weights)
        # print(quotient)

        paragraphs *= weights #Consider removing this operation for comparison
        paragraphVectors = np.sum(paragraphs, axis=1)

        paragraphVectors /= quotient

        paragraphVectors = self.removePrimaryComponent(paragraphVectors)
        #print('paragraph vectors: ',paragraphVectors[0])

        return paragraphVectors
