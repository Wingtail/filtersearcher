import numpy as np
from sklearn.metrics.pairwise import euclidean_distances
from sklearn.feature_extraction.text import CountVectorizer
import pickle
import unkModel
import string

class WMD():
    def __init__(self, directory='gloveDic.pckl'):
        self.vocabDict = pickle.load(open(directory, 'rb'))
        self.vectorizer = CountVectorizer(stop_words='english')
        self.unkModel = unkModel.UnkModel()

    def getMinEuclideanDist(self,a,b): #Assume a and b are matrices
         dist = euclidean_distances(a,b)
         return np.amin(dist, axis=1)

    def getRWMD(self,texts): #Assume texts is list
        # tokenize and build vocab

        texts = [text.translate(str.maketrans('', '', string.punctuation)) for text in texts]
        vector = self.vectorizer.fit_transform(texts).toarray().astype(float)

        query = vector[0] #Assuming that query is in the first index

        documents = np.delete(vector, (0), axis=0)

        queryIndices = np.nonzero(query)[0]

        vocabWords = self.vectorizer.get_feature_names()

        queryWords = [vocabWords[i] for i in queryIndices]

        queryVector = np.zeros((len(queryWords), 300))

        indices = []
        unkWords = []

        for i in range(len(queryWords)):
            if(queryWords[i] in self.vocabDict):
                queryVector[i] = self.vocabDict.__getitem__(queryWords[i])
            else:
                indices.append(i)
                unkWords.append(queryWords[i])

        unkWords = self.unkModel.getUnkVector(unkWords)
        for i in range(len(indices)):
            queryVector[indices[i]] = unkWords[i]

        quotient_query = np.sum(query)
        quotient_document = np.sum(documents, axis=1, keepdims=True)

        query /= quotient_query
        documents_nBOW = documents / quotient_document

        query_nBOW = query[query != 0.0]


        vocabVec = np.zeros((len(vocabWords), 300))

        indices = []
        unkWords = []

        for i in range(len(vocabWords)):
            if(vocabWords[i] in self.vocabDict):
                vocabVec[i] = self.vocabDict[vocabWords[i]]
            else:
                indices.append(i)
                unkWords.append(vocabWords[i])

        unkWords = self.unkModel.getUnkVector(unkWords)
        for i in range(len(indices)):
            vocabVec[indices[i]] = unkWords[i]

        distances = euclidean_distances(queryVector, vocabVec)

        distances = np.tile(distances, len(documents))

        mask = documents.flatten()
        mask = np.expand_dims((mask==0).astype(float) * 1e9, axis=0)
        distances += mask

        distances = np.reshape(distances, (len(queryVector), len(documents), len(vocabWords)))

        distances = np.transpose(distances, (1,0,2))

        doc_min = np.nanmin(distances,axis=1)
        query_min = np.nanmin(distances,axis=2)

        if(len(query_min.shape) < 2):
            minDistances_query = np.transpose(np.expand_dims(query_min, axis=0), (1,0))
        else:
            minDistances_query = np.transpose(query_min, (1,0))

        if(len(doc_min.shape) < 2):
            minDistances_document = np.expand_dims(np.expand_dims(doc_min, axis=0), axis=2)
        else:
            minDistances_document = np.expand_dims(np.squeeze(doc_min), axis=2)

        RWMD_q = np.matmul(query_nBOW, minDistances_query)
        RWMD_d = np.squeeze(np.matmul(np.expand_dims(documents_nBOW, axis=1), minDistances_document))

        return np.maximum(RWMD_q, RWMD_d)
